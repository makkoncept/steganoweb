# [Live Demo](http://steganoweb.ml/)
# steganoweb
basic web wrapper of the amazing [stegano](https://github.com/cedricbonhomme/Stegano) library.

## Run locally

- clone the repo
```
git clone https://github.com/makkoncept/steganoweb.git
cd steganoweb
```
- create a virtual environment and install requirements
```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```
- run the development server
```
python run.py
```
visit http://localhost:5000/ 
